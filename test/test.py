"""
In this file unittests of the critical algorithms are performed.

"""
import numpy as np
import unittest
import sys
import time
sys.path.append('../')
from lorenz import *


class test_cases(unittest.TestCase):

    def set_up(self):
        """
        Nothing to setup
        """
        pass

    def test_1_calc_lorenz(self):
        """
        Test of the ODEs solvers
        """
        self.assertEqual(solver.calc_lorenz(10, 2, 16, 1.5, 0.5, 0.6),
                         (-10.0, 1.6, -8.85))
        self.assertEqual(solver.calc_lorenz(10, 2, 16, 0.5, 1.5, 0.2),
                         (10.0, -0.6, -2.45))
        self.assertEqual(solver.calc_lorenz(0.5, 0.2, 1, -5, -0.5, 2),
                         (2.25, 9.5, 0.5))

    def test_2_simulate_atmospheric_convection(self):
        """
        Testing that the output of simulation is of correct length
        and that the initial value is correct in the output simulation.
        """
        sigma = 14
        beta = 2.6667
        rho = 28
        dt = 0.01
        N = 20000

        x0 = 5.0
        y0 = 1
        z0 = 2

        diffX = np.empty(N + 1)
        diffY = np.empty(N + 1)
        diffZ = np.empty(N + 1)

        # Setting the intial values in numpy array
        diffX[0], diffY[0], diffZ[0] = (x0, y0, z0)

        X, Y, Z = solver.simulate_atmospheric_convection(
            sigma, rho, beta, dt, diffX, diffY, diffZ)

        self.assertEqual(len(X), (N + 1))
        self.assertEqual(len(Y), (N + 1))
        self.assertEqual(len(Z), (N + 1))

        self.assertEqual(X[0], x0)
        self.assertEqual(Y[0], y0)
        self.assertEqual(Z[0], z0)

    def test_3_load_json_init_values(self):
        """
        Make sure our config file contains appropiate data
        """
        default_config, cases = filehandling.load_config(
            "../configuration/config.json")
        self.assertIn('dt', default_config)
        self.assertIn('samples', default_config)
        self.assertIn('initValues', default_config)
        self.assertIn('x0', default_config['initValues'])
        self.assertIn('y0', default_config['initValues'])
        self.assertIn('z0', default_config['initValues'])

    def test_4_load_json_test_cases(self):
        """
        Make sure test cases have appropiate data
        """
        default_config, cases = filehandling.load_config(
            "../configuration/config.json")

        for key, config in cases.items():
            self.assertIn('sigma', config)
            self.assertIn('beta', config)
            self.assertIn('rho', config)


if __name__ == '__main__':
    unittest.main()
