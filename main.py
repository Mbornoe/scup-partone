"""
This is the main functon that handles caluclation and plots for
the mini-project in the "Scientific Computing using Python
 Part-one" Phd Course held at Aalborg University summer 2016.
"""
import sys
import time
import argparse
import os
from lorenz import *


def main(args):
    if not os.path.isfile(str(args.config)):
        print("Error: The config file %s  does not exist." % args.config)
        exit()
    if (args.all):
        run.simulate_behavior_all(args.config)

    if (args.all and args.plot):
        run.plot_all_cases(args.config)

    if (args.testcase):
        for case in args.testcase:
            run.simulate_case_behavior(case, args.config)

    if (args.testcase and args.plot):
        for case in args.testcase:
            run.plot_one_case(case, args.config)


if __name__ == '__main__':
    """
    If run.py is used as main function it will run the desired
    experiment and plots for the "Scientific Computing using
    Python Part-one" Phd Course.
    """

    parser = argparse.ArgumentParser(description='This program takes a json formatted\
     configuration file and simulates the Lorenz Attractor for Atmospheric\
     convection based on 3 Ordinary Differential Equations (ODEs).\
     The configuration file must contain appropiate default parameters\
     aswell as test cases. When executing the test case simulation,\
     the files will be stored in belonging cases depended folder in the\
     "cases" directory.\n\n \
     Example of use:\n \
     python main.py -c "configuration/config.json" -tc 1 -tc 3 -tc 5 -p \n\
     python main.py -c "configuration/config.json" -a -p',
                                     epilog='Developed in summer 2016 by Morten Bornoe Jensen for the\
     Scientific Computing using Pythong - Part One\n',
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('-c', '--config', metavar='"configuration/\
                        config.json"',
                        default='"configuration/config.json"',
                        type=str, help='Path to the json formatted\
                         config file.')
    parser.add_argument('-a', '--all', action='store_true',
                        help='Calculate all testcases')
    parser.add_argument('-tc', '--testcase', type=int,
                        action='append', help='Calculate specific testcase')
    parser.add_argument('-p', '--plot', action='store_true',
                        help='Create plots for belongning testcases')

    args = parser.parse_args()
    main(args)
