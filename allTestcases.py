"""
This is the main functon that handles caluclation and plots for
the mini-project in the "Scientific Computing using Python
 Part-one" Phd Course held at Aalborg University summer 2016.
"""
import sys
import time
from lorenz import *

def run_simulation(args):
    run.simulate_case_behavior(args.testcase,'configuration/config.json')
    run.plot_one_case(args.testcase,'configuration/config.json')
    pass
if __name__ == '__main__':
    """
    If run.py is used as main function it will run the desired
    experiment and plots for the "Scientific Computing using
    Python Part-one" Phd Course.
    """

    parser = argparse.ArgumentParser(description='Simluate and plot the Lorenz Attractor', epilog='This program will create a several several new files, however the \'results.csv\' and the plot are the most interesting, as it contains the evalutation results. Example of use: python calcprcTL.py -gt frameAnnotationsBULB.csv -d go.csv -p 0.5 -o -ptf prcPlot -c "go" -l "AFC"')
    
    parser.add_argument('-c','--config', metavar='configuration/config.json', type=str, help='Path to the json formatted config file.')
    parser.add_argument('-a','--all',action='store_true','Calculate all testcases')
    parser.add_argument('-tc','--testcase',type=int,'Calculate specific testcase')
    parser.add_argument('-p','--plot', action='store_true', help='Create plots for belongning testcases')

    args = parser.parse_args()

    run_simulation(args)

    
    # simulate_case_behavior(2)
    # plot_one_case(2)
    # simulate_case_behavior(3)
    # plot_one_case(3)
    # simulate_case_behavior(4)
    # plot_one_case(4)
    # simulate_case_behavior(5)
    # plot_one_case(5)


    #simulate_behavior()
    #plot_all_cases()
