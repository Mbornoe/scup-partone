"""
This file can contain functionalities for saving/loading data

"""
import json
import os


def read_configfile(configPath):
    """
    Function that loads json formatted config file

    Args:
        configPath: Path to configuration file
    """
    with open(configPath, 'r') as f:
        config = json.load(f)
    return config


def sort_config(config):
    """
    Function splits json formatted config into 2 dicts

    Args:
        config: A dict object (e.g. output from read_configfile)
    """
    initConditions = config['defaultSettings']
    allCases = config['cases']
    return initConditions, allCases


def load_config(configPath):
    """
    Interface function thats caretakes reading in config and sorting it.

    Args:
        configPath: Path to configuration file
    """
    jsonConfig = read_configfile(configPath)
    return sort_config(jsonConfig)


def print_config(config):
    """
    Function for printing the allCases dictionaries.

    Args:
        config: An allCases dictionaries (e.g. output from sort_config)
    """
    if type(config) == dict:
        for keys, values in config.items():
            print 'Case %s : %s' % (keys, values)
    else:
        print config


def create_case_filepath(filename):
    """
    Function for creating case filepath name

    Args:
        filename: String containing info of case and case number
    """
    return "cases/" + filename


def save_to_file(filename, X, Y, Z):
    """
    Function saving results in one json formatted file.
    The file is saved into the specific case folder.

    Args:
        filename: Desired filename
        X: Numpy array with ODE-x results
        Y: Numpy array with ODE-y results
        Z: Numpy array with ODE-z results
    """
    results = {
        'X': list(X),
        'Y': list(Y),
        'Z': list(Z)
    }
    if not os.path.isdir(create_case_filepath(filename)):
        os.makedirs(create_case_filepath(filename))

    with open((create_case_filepath(filename) + '/data.json'), 'w') as fp:
        json.dump(results, fp, indent=4)


def load_json_results(path):
    """
    Function that loads results from one json formatted file.

    Args:
        path: Directory path of desired json file.
    """
    results = json.loads(open((path + '/data.json')).read())
    return results['X'], results['Y'], results['Z']
