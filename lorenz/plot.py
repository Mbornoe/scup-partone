"""
This file contains functionalities for plotting all the
required plots defined in the mini-project
"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import filehandling


def plot_3d(xs, ys, zs, case, config):
    """
    Function that generate a 3D plot based 3 inputs.
    The 3D plot will be saved in the specific case folder.

    Args:
        xs: A list containing x-axis data
        ys: A list containing y-axis data
        zs: A list containing z-axis data
        case: An integer defining the case number
        config: A dictionary with case configratuion
    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.plot(xs, ys, zs)
    ax.set_xlabel("X-Axis")
    ax.set_ylabel("Y-Axis")
    ax.set_zlabel("Z-Axis")

    title = "Lorenz Attractor 3D plot: Case-%s (Beta:%s Rho:%s Sigma:%s)"\
        % (case, config['beta'], config['rho'], config['sigma'])

    ax.set_title(title)

    plot_filename = filehandling.create_case_filepath("case-" + case)\
        + "/plot3D.pdf"

    fig.savefig(plot_filename, format='pdf')


def plot_2d(xAxis, yAxis, case, config, labels):
    """
    Function that generate a 2D plot based 2 inputs.
    The 2D plot will be saved in the specific case folder.

    Args:
        xs: A list containing x-axis data
        ys: A list containing y-axis data
        zs: A list containing z-axis data
        case: An integer defining the case number
        config: A dictionary with case configratuion
        labels: A list containing axis labels (e.g. x and y)
    """
    fig = plt.figure()
    ax = fig.gca()

    ax.plot(xAxis, yAxis)
    ax.set_xlabel((labels[0] + "-axis"))
    ax.set_ylabel((labels[1] + "-axis"))

    title = "Lorenz Attractor 2D plot(%s-%s): Case-%s (Beta:%s Rho:%s\
            Sigma:%s)" % (labels[0], labels[1], case, config['beta'],
                          config['rho'], config['sigma'])

    ax.set_title(title)

    plot_filename = filehandling.create_case_filepath("case-" + case)\
        + "/plot2D-" + labels[0] + labels[1] + ".pdf"

    fig.savefig(plot_filename, format='pdf')


def plot_2d_all(xs, ys, zs, case, config):
    """
    Function that generate a 2D plot with 3 subplots
    The 3D plot will be saved in the specific case folder.

    Args:
        xs: A list containing x-axis data
        ys: A list containing y-axis data
        zs: A list containing z-axis data
        case: An integer defining the case number
        config: A dictionary with case configratuion
    """
    f, axarr = plt.subplots(1, 3, sharex=True, sharey=True)
    axarr[0].plot(xs, ys)
    axarr[0].set_xlabel("X-Axis")
    axarr[0].set_ylabel("Y-Axis")
    axarr[0].set_title = "Case-%s (Beta:%s Rho:%s Sigma:%s)"\
        % (case, config['beta'], config['rho'], config['sigma'])

    axarr[1].plot(xs, zs)
    axarr[1].set_xlabel("X-Axis")
    axarr[1].set_ylabel("Z-Axis")
    axarr[1].set_title = "Case-%s (Beta:%s Rho:%s Sigma:%s)"\
        % (case, config['beta'], config['rho'], config['sigma'])

    axarr[2].plot(ys, zs)
    axarr[2].set_xlabel("Y-Axis")
    axarr[2].set_ylabel("Z-Axis")
    axarr[2].set_title = "Case-%s (Beta:%s Rho:%s Sigma:%s)"\
        % (case, config['beta'], config['rho'], config['sigma'])

    plot_filename = filehandling.create_case_filepath("case-" + case)\
        + "/plot2D_all.pdf"

    f.savefig(plot_filename, format='pdf')
