"""
This file may contain utility functionalities to the extend you will need it

"""


def convert_string_to_frac(fraction):
    """
    Function that converts a string to a fraction.

    Args:
        fraction: Input string (e.g. "8/3")
    """
    if type(fraction) == unicode or type(fraction) == str:
        frac = fraction.split("/")
        return float(frac[0]) / float(frac[1])
    else:
        return fraction
