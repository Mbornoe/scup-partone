"""
This file contains convenient interface/function for

1: computing a trajectory using an ODE solver from solver.py
2: save data to file
3: load data from file
4: plot data

"""
import sys
import time
sys.path.append('../')
from lorenz import *


def save_data_to_file(caseNumber, lorenzX, lorenzY, lorenzZ):
    """
    Interface function that will store 3 numpy arrays with data

    Args:
        caseNumber: Casenumber which will dictate location path
        lorenzX: Numpy array with ODE-x results
        lorenzY: Numpy array with ODE-y results
        lorenzZ: Numpy array with ODE-z results
    """
    filehandling.save_to_file(("case-" + caseNumber),
                              lorenzX, lorenzY, lorenzZ)


def load_data_from_file(filepath):
    """
    Interface function that load in a json file with results(x,y,z)

    Args:
        filepath: Filepath to the json results file
    """
    return filehandling.load_json_results(filepath)


def plot_data_2d(xs, ys, zs, case, config):
    """
    Interface function that plots the desired 2D plots

    Args:
        xs: A list containing x-axis data
        ys: A list containing y-axis data
        zs: A list containing z-axis data
        case: An integer defining the case number
        config: A dictionary with case configratuion
        labels: A list containing axis labels (e.g. x and y)
    """
    plot.plot_2d(xs, ys, case, config, ["X", "Y"])
    plot.plot_2d(xs, zs, case, config, ["X", "Z"])
    plot.plot_2d(ys, zs, case, config, ["Y", "Z"])


def plot_data_3d(xs, ys, zs, case, config):
    """
    Interface function that plots the desired 3D plot

    Args:
        xs: A list containing x-axis data
        ys: A list containing y-axis data
        zs: A list containing z-axis data
        case: An integer defining the case number
        config: A dictionary with case configratuion
    """
    plot.plot_3d(xs, ys, zs, case, config)


def plot_data_2d_all(xs, ys, zs, case, config):
    """
    Interface function that plots the desired 2D plots
    All 2D plots will be put into 1 figure using subplots

    Args:
        xs: A list containing x-axis data
        ys: A list containing y-axis data
        zs: A list containing z-axis data
        case: An integer defining the case number
        config: A dictionary with case configratuion
        labels: A list containing axis labels (e.g. x and y)
    """
    plot.plot_2d_all(xs, ys, zs, case, config)


def plot_all_cases(configpath="configuration/config.json"):
    """
    Interface handler function that plots all required plots

    Args:
        xs: A list containing x-axis data
        ys: A list containing y-axis data
        zs: A list containing z-axis data
        case: An integer defining the case number
        config: A dictionary with case configratuion
        labels: A list containing axis labels (e.g. x and y)
    """
    initConditions, cases = filehandling.load_config(configpath)
    print "Plotting results..."
    for key, config in cases.items():
        xs, ys, zs = load_data_from_file(filehandling.create_case_filepath
                                         ("case-" + key))

        plot_data_3d(xs, ys, zs, key, config)
        plot_data_2d_all(xs, ys, zs, key, config)
        plot_data_2d(xs, ys, zs, key, config)


def plot_one_case(key,configpath="configuration/config.json"):
    """
    Interface handler function that plots one case

    Args:
        xs: A list containing x-axis data
        ys: A list containing y-axis data
        zs: A list containing z-axis data
        case: An integer defining the case number
        config: A dictionary with case configratuion
        labels: A list containing axis labels (e.g. x and y)
    """
    initConditions, cases = filehandling.load_config(configpath)
    print "Plotting results..."
    xs, ys, zs = load_data_from_file(filehandling.create_case_filepath("case-" + str(key)))
    plot_data_3d(xs, ys, zs, str(key), cases[str(key)])
    plot_data_2d_all(xs, ys, zs, str(key), cases[str(key)])
    plot_data_2d(xs, ys, zs, str(key), cases[str(key)])


def simulate_behavior_all(configpath="configuration/config.json"):
    """
    Interface handler function that caretakes the entire simulation
    for all configs.

    Args:
        configpath: path to the json formatted config file
    """
    initConditions, cases = filehandling.load_config(configpath)

    for key, config in cases.items():
        time1 = time.time()
        print "------------------"
        print "Simulating CASE-" + key
        print "\tBeta:\t" + str(config['beta'])
        print "\tSigma:\t" + str(config['sigma'])
        print "\tRho:\t" + str(config['rho'])

        lorenzX, lorenzY, lorenzZ = lorenz.solver.lorenz_ode(initConditions, config)
        time2 = time.time()
        print "Executed in  took %0.3f ms" % ((time2-time1)*1000.0)

        time1 = time.time()
        save_data_to_file(key, lorenzX, lorenzY, lorenzZ)
        time2 = time.time()

        saveString = filehandling.create_case_filepath("case-" + key)\
            + "' in %0.3f ms" % ((time2 - time1) * 1000.0)
        print "Saved results to: '" + saveString

        print "\n"


def simulate_case_behavior(key,configpath="configuration/config.json"):
    """
    Interface handler function that caretakes simulation of 1

    Args:
        configpath: path to the json formatted config file
    """
    initConditions, cases = filehandling.load_config(configpath)

    time1 = time.time()
    print "------------------"
    print "Simulating CASE-" + str(key)
    print "\tBeta:\t" + str(cases[str(key)]['beta'])
    print "\tSigma:\t" + str(cases[str(key)]['sigma'])
    print "\tRho:\t" + str(cases[str(key)]['rho'])

    lorenzX, lorenzY, lorenzZ = lorenz.solver.lorenz_ode(initConditions, cases[str(key)])
    time2 = time.time()
    print "Executed in  took %0.3f ms" % ((time2-time1)*1000.0)

    time1 = time.time()
    save_data_to_file(str(key), lorenzX, lorenzY, lorenzZ)
    time2 = time.time()

    saveString = filehandling.create_case_filepath("case-" + str(key))\
        + "' in %0.3f ms" % ((time2 - time1) * 1000.0)
    print "Saved results to: '" + saveString

    print "\n"

