"""
This file contains the ODE solver

"""
import numpy as np
import util
from numba import jit


@jit
def calc_lorenz(sigma, rho, beta, x, y, z):
    """
    Function that calculates the atmospheric convection using on
    lorenz attractor which is based on three firster order
    Ordinary Differential Equeations(ODEs).
    This function is runned through a LLVM just-in-time (JIT)
    compiler using Numba which significantly increases performance.

    Args:
        sigma: Lorenz attractor model parameter
        rho: Lorenz attractor model parameter
        beta: Lorenz attractor model parameter
        x: x-position in space
        y: y-position in space
        z: z-position in space

    >>> calc_lorenz(10,2,16,1.5,0.5,0.6)
    (-10.0, 1.6, -8.85)

    >>> calc_lorenz(10,2,16,0.5,1.5,0.2)
    (10.0, -0.6, -2.45)
    
    """
    dxdt = sigma*(y - x)
    dydt = rho*x - y - x*z
    dzdt = x*y - beta*z

    return dxdt, dydt, dzdt


@jit
def simulate_atmospheric_convection(sigma, rho, beta, dt, diffX, diffY, diffZ):
    """
    Function that simulates the Lorenz attractor ODEs
    in a discrete manner.
    This function is runned through a LLVM just-in-time (JIT)
    compiler using Numba which significantly increases performance.

    Args:
        sigma: Lorenz attractor model parameter
        rho: Lorenz attractor model parameter
        beta: Lorenz attractor model parameter
        diffX: ODE x-position
        diffY: ODE y-position
        diffZ: ODE z-position
    """
    for i in range(diffX.size-1):
        dxdt, dydt, dzdt = calc_lorenz(sigma, rho, beta, diffX[i], diffY[i],
                                       diffZ[i])
        diffX[i+1] = diffX[i] + (dt * dxdt)
        diffY[i+1] = diffY[i] + (dt * dydt)
        diffZ[i+1] = diffZ[i] + (dt * dzdt)

    return diffX, diffY, diffZ


def lorenz_ode(initConditions, config):
    """
    Interface function that simulates the Lorenz attractor ODEs
    in a discrete manner.

    Args:
        initConditions: Dictionary containing intial parameters
        config: Dictionary containing case depended case configurations
    """
    sigma = config['sigma']
    beta = util.convert_string_to_frac(config['beta'])
    rho = config['rho']
    dt = initConditions['dt']
    N = initConditions['samples']
    x0 = initConditions['initValues']['x0']
    y0 = initConditions['initValues']['y0']
    z0 = initConditions['initValues']['z0']

    # Allocating empty numpy arrays with size of
    # sample size + 1 for coping with intial value
    diffX = np.empty(N+1)
    diffY = np.empty(N+1)
    diffZ = np.empty(N+1)

    # Setting the intial values in numpy array
    diffX[0], diffY[0], diffZ[0] = (x0, y0, z0)

    return simulate_atmospheric_convection(sigma,
                                           rho, beta, dt, diffX, diffY, diffZ)


if __name__ == "__main__":
    # python -m doctest -v solver.py
    import doctest
    doctest.testmod()
