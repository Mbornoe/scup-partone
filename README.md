# Scientific Computing using Python, part 1 mini-project
A repository containing the mini-project for the PhD course "Scientific Computing using Python, part 1", held at Aalborg University, May 2016. 

# Introduction
This python project relates to the Lorenz attractor, which simulates the atmospheric convection based on three coupled first order Ordinary Differential Equations (ODEs). 

# Prerequisites
If you pull the entire project, you should be able to execute below code examples. If you however wish to mange changes to configuration, you have to pay attention to the config.json file located in the configuration folder.
If additional test cases are wanted, the code are developed such it scales accordingly and creates appropiate folders for new test cases.
If you want to create entirely new test cases, make sure to meet the json format seen below:

```python
{
    "defaultSettings": {
        "dt":0.01, "samples":10000,
        "initValues": {"x0": 0.1, "y0": 0 , "z0":0 }
    },
    "cases": {
        "1": {"sigma":10, "beta":"8/3", "rho":6},
        "2": {"sigma":10, "beta":"8/3", "rho":16},
        "3": {"sigma":10, "beta":"8/3", "rho":28},
        "4": {"sigma":14, "beta":"8/3", "rho":28},
        "5": {"sigma":14, "beta":"13/3", "rho":28},
        "6": {"sigma":18, "beta":"13/3", "rho":32}
    }
}
```

# Code example
There are two ways of executing the code. First approach if you want to simulate all the test cases included in your config file, the second approach is if you want to specify which test case you wish to simulate.
```python
# Executes all test cases and plot them
python main.py -c "configuration/config.json" -a -p

# Executes test case 1, 3, 5 and plots them
python main.py -c "configuration/config.json" -tc 1 -tc 3 -tc 5 -p
```
The results of running either of above approach is that the desired test cases are simulated and whereafter the results are stored in the 'cases' folder in a specific case number folder. For each case the data is saved in a json formatted file, with a key for X, Y, Z, respectively. This makes it easy for other to load in and split the data into their system.
Furthermore, if the -p flag are used, 5 different plots are made. 

# Tests
The projects contains unittests that verify that the ODEs are working as desired and that the test cases are loaded in correctly from the json formatted files.
The unittest can be executed by navigating to the test folder and executing test.py.
```python
# Navigate to test folder
cd test

# Executes unittest
python test.py
```

# Contributors 
The code is developed by Morten Bornø Jensen from Aalborg University in summer 2016.

